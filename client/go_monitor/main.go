package main

import (
	//"container/list"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	//"time"

	"github.com/julienschmidt/httprouter"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/mem"
	"github.com/shirou/gopsutil/net"
	"github.com/shirou/gopsutil/process"
)

func Decimal(value float64) float64 {
	value, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", value), 64)
	return value
}

func StructJson(sc interface{}) string {
	data, err := json.Marshal(&sc)
	if err != nil {
		fmt.Println("错误序列化信息是", err)
	}
	return string(data)

}

//进程数
func getProcess() int {
	pids, _ := process.Pids()
	process_num := len(pids)
	return process_num
}

//CPU
func getCpuInfo() interface{} {
	cpuInfos, _ := cpu.Info()
	//ts := StructJson(cpuInfos[0])
	return cpuInfos[0]
}

// 负载信息
func getLoadInfo() interface{} {

	info, _ := load.Avg()
	//ts := StructJson(info)
	return info
}

// 内存信息
func getMemInfo() interface{} {
	memInfo, _ := mem.VirtualMemory()
	//ts := StructJson(memInfo)
	return memInfo
}

// 主机信息
func getHostInfo() interface{} {
	hostInfo, _ := host.Info()
	//ts := StructJson(hostInfo)
	return hostInfo
}

// 磁盘信息
func getDiskInfo() interface{} {
	var l = []interface{}{}

	parts, _ := disk.Partitions(true)
	// 打印磁盘信息

	for _, part := range parts {
		partInfo, _ := disk.Usage(part.Mountpoint)
		ds := map[string]interface{}{}
		ds["path"] = part.Mountpoint

		var pud = Decimal(float64(partInfo.Used/1024/1024) / 1000)
		ds["usedinfo_G"] = pud
		var pup = Decimal(partInfo.UsedPercent)
		ds["UsedPercent"] = pup

		//ds["usedinfo"] = float64(partInfo.Used)
		//ds["UsedPercent"] = partInfo.UsedPercent
		//fmt.Println(reflect.TypeOf(partInfo.Used))
		//fmt.Println(reflect.TypeOf(partInfo.UsedPercent))

		tmp := ds
		//fmt.Println(ds)
		l = append(l, &tmp)
		//fmt.Println(l)

	}

	//ts := StructJson(l)
	return l
}

// 网络信息
func getNetInfo() interface{} {
	info, _ := net.IOCounters(true)
	nets := map[string]interface{}{}
	for _, v := range info {

		//net.IOCountersStat类型转换map
		b, _ := json.Marshal(&v)
		var m map[string]interface{}
		_ = json.Unmarshal(b, &m)
		//fmt.Println(m["name"])
		//fmt.Println(m)
		//fmt.Println(reflect.TypeOf(m["name"]))
		n, ok := m["name"].(string)
		if ok {
			nets[n] = m
		}

	}
	return nets
}

func BaseMonitor(w http.ResponseWriter, r *http.Request, v httprouter.Params) {
	switch {
	case v.ByName("haoge") == "getCpuInfo":
		fmt.Fprintf(w, StructJson(getCpuInfo()))

	case v.ByName("haoge") == "getLoadInfo":
		fmt.Fprintf(w, StructJson(getLoadInfo()))

	case v.ByName("haoge") == "getMemInfo":
		fmt.Fprintf(w, StructJson(getMemInfo()))

	case v.ByName("haoge") == "getHostInfo":
		fmt.Fprintf(w, StructJson(getHostInfo()))

	case v.ByName("haoge") == "getDiskInfo":
		fmt.Fprintf(w, StructJson(getDiskInfo()))

	case v.ByName("haoge") == "getNetInfo":
		fmt.Fprintf(w, StructJson(getNetInfo()))

	case v.ByName("haoge") == "all":

		all_mo := map[string]interface{}{}

		all_mo["getCpuInfo"] = getCpuInfo()
		all_mo["getLoadInfo"] = getLoadInfo()
		all_mo["getMemInfo"] = getMemInfo()
		all_mo["getHostInfo"] = getHostInfo()
		all_mo["getDiskInfo"] = getDiskInfo()
		all_mo["getNetInfo"] = getNetInfo()
		all_mo["getProcess"] = getProcess()

		fmt.Fprintf(w, StructJson(all_mo))

	}
}

func main() {
	router := httprouter.New()
	//router.GET("/", Index)
	router.GET("/BaseMonitor/:haoge", BaseMonitor)

	log.Fatal(http.ListenAndServe(":6789", router))
}
