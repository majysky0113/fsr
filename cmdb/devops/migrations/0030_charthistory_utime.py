# Generated by Django 2.0.6 on 2021-08-19 16:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('devops', '0029_charthistory'),
    ]

    operations = [
        migrations.AddField(
            model_name='charthistory',
            name='utime',
            field=models.DateTimeField(auto_now=True, null=True, verbose_name='更新时间'),
        ),
    ]
