import psutil
import json
from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User, Group
from resources.models import NewServer
from devops.models import AutoReCovery,MonitorConfig
from alert.models import AlertHistory
from users.models import Profile
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render_to_response,HttpResponse
# Create your views here.

class IndexView(LoginRequiredMixin, TemplateView):
	template_name = 'public/layout.html'
	def get(self,request):
		try:
			username = request.session['username']
		except:
			return HttpResponseRedirect('/users/login/')
		return render_to_response('public/layout.html',locals())

class IndexHome(LoginRequiredMixin, TemplateView):
	template_name = 'index_home.html'
	def get(self,request):
		user_count = User.objects.all().count()
		group_count = Group.objects.all().count()
		host_count = NewServer.objects.all().count()
		autorecovery_count = AutoReCovery.objects.all().count()
		monitorconfig_count = MonitorConfig.objects.all().count()
		alerthistory_count = AlertHistory.objects.all().count()
		host_error_count = NewServer.objects.filter(scan_status=0).count()
		server_list = NewServer.objects.all().order_by('-update_date')
		return render_to_response('index_home.html',locals())

class InitJson(LoginRequiredMixin, TemplateView):
	template_name = 'index.html'

	def get(self, request):

		czy = {
				"title": "FSR",
				"icon": "fa fa-address-book",
				"href": "",
				"flag": 1,
				"target": "_self",
				"child": [
				  {
					"title": "用户管理",
					"href": "",
					"icon": "fa fa-home",
					"target": "_self",
					"child": [
					  {
						"title": "用户管理",
						"href": "/users/user/list/",
						"icon": "fa fa-tachometer",
						"target": "_self"
					  },
					  {
						"title": "用户组管理",
						"href": "/users/group/list/",
						"icon": "fa fa-tachometer",
						"target": "_self"
					  }
					]
				  },
				  {
					"title": "资产管理",
					"href": "",
					"icon": "fa fa-calendar",
					"target": "_self",
					"child": [
					  {
						"title": "资产主机",
						"href": "/resources/servers/list/",
						"icon": "fa fa-list-alt",
						"target": "_self"
					  },
					  {
						"title": "资产组",
						"href": "/resources/group/list/",
						"icon": "fa fa-navicon",
						"target": "_self"
					  },
					  {
						"title": "资产用户",
						"href": "/resources/serveruser/list/",
						"icon": "fa fa-navicon",
						"target": "_self"
					  }
					]
				  },
				  {
					"title": "故障自愈",
					"href": "",
					"icon": "fa fa-flag-o",
					"target": "_self",
					"child": [
					  {
						"title": "自监控自愈",
						"href": "/devops/autorecovery/",
						"icon": "fa fa-stumbleupon-circle",
						"target": "_self"
					  },
					  {
						"title": "Prometheus",
						"href": "/devops/prometheus/",
						"icon": "fa fa-stumbleupon-circle",
						"target": "_self"
					  }
					]
				  },
				  {
					"title": "监控中心",
					"href": "",
					"icon": "fa fa-calendar",
					"target": "_self",
					"child": [
					  {
						"title": "探点监控",
						"href": "/devops/monitor_config/",
						"icon": "fa fa-list-alt",
						"target": "_self"
					  },
					  {
						"title": "主机监控",
						"href": "/devops/chart_list/",
						"icon": "fa fa-navicon",
						"target": "_self"
					  },
					  {
						"title": "MySQL监控",
						"href": "/dbmonitor/mysql_dblist/",
						"icon": "fa fa-navicon",
						"target": "_self"
					  }
					]
				  },
				  {
					"title": "报警中心",
					"href": "",
					"icon": "fa fa-calendar",
					"target": "_self",
					"child": [
					  {
						"title": "报警自愈历史",
						"href": "/alert/alert_history/",
						"icon": "fa fa-list-alt",
						"target": "_self"
					  },
					  {
						"title": "报警联系人",
						"href": "/alert/alert_linkperson/",
						"icon": "fa fa-navicon",
						"target": "_self"
					  },
					  {
						"title": "报警联系组",
						"href": "/alert/alert_linkgroup/",
						"icon": "fa fa-navicon",
						"target": "_self"
					  }
					]
				  },
				  {
					"title": "系统管理",
					"href": "",
					"icon": "fa fa-flag-o",
					"target": "_self",
					"child": [
					  {
						"title": "系统设置",
						"href": "/alert/attentionconfig/",
						"icon": "fa fa-stumbleupon-circle",
						"target": "_self"
					  }
					]
				  }
				]
			  }

		jsdata = {
			"homeInfo": {
			  "title": "首页",
			  "href": "/dashboard/index_home"
			},
			"logoInfo": {
			  "title": "FSR",
			  "image": "/static/layuimini/images/logo.png",
			  "href": ""
			},
			"menuInfo": [

			]
		  }


		jsdata["menuInfo"].append(czy)
		return JsonResponse(jsdata)


class UserAPIView(LoginRequiredMixin, TemplateView):
	template_name = 'index.html'

	def get(self, request):
		jsdata = {
			  "code": 0,
			  "msg": "",
			  "count": 1000,
			  "data": [
				{
				  "id": 10000,
				  "username": "user-0",
				  "sex": "女",
				  "city": "城市-0",
				  "sign": "签名-0",
				  "experience": 255,
				  "logins": 24,
				  "wealth": 82830700,
				  "classify": "作家",
				  "score": 57
				},
				{
				  "id": 10001,
				  "username": "user-1",
				  "sex": "男",
				  "city": "城市-1",
				  "sign": "签名-1",
				  "experience": 884,
				  "logins": 58,
				  "wealth": 64928690,
				  "classify": "词人",
				  "score": 27
				},
				{
				  "id": 10002,
				  "username": "user-2",
				  "sex": "女",
				  "city": "城市-2",
				  "sign": "签名-2",
				  "experience": 650,
				  "logins": 77,
				  "wealth": 6298078,
				  "classify": "酱油",
				  "score": 31
				},
				{
				  "id": 10003,
				  "username": "user-3",
				  "sex": "女",
				  "city": "城市-3",
				  "sign": "签名-3",
				  "experience": 362,
				  "logins": 157,
				  "wealth": 37117017,
				  "classify": "诗人",
				  "score": 68
				},
				{
				  "id": 10004,
				  "username": "user-4",
				  "sex": "男",
				  "city": "城市-4",
				  "sign": "签名-4",
				  "experience": 807,
				  "logins": 51,
				  "wealth": 76263262,
				  "classify": "作家",
				  "score": 6
				},
				{
				  "id": 10005,
				  "username": "user-5",
				  "sex": "女",
				  "city": "城市-5",
				  "sign": "签名-5",
				  "experience": 173,
				  "logins": 68,
				  "wealth": 60344147,
				  "classify": "作家",
				  "score": 87
				},
				{
				  "id": 10006,
				  "username": "user-6",
				  "sex": "女",
				  "city": "城市-6",
				  "sign": "签名-6",
				  "experience": 982,
				  "logins": 37,
				  "wealth": 57768166,
				  "classify": "作家",
				  "score": 34
				},
				{
				  "id": 10007,
				  "username": "user-7",
				  "sex": "男",
				  "city": "城市-7",
				  "sign": "签名-7",
				  "experience": 727,
				  "logins": 150,
				  "wealth": 82030578,
				  "classify": "作家",
				  "score": 28
				},
				{
				  "id": 10008,
				  "username": "user-8",
				  "sex": "男",
				  "city": "城市-8",
				  "sign": "签名-8",
				  "experience": 951,
				  "logins": 133,
				  "wealth": 16503371,
				  "classify": "词人",
				  "score": 14
				},
				{
				  "id": 10009,
				  "username": "user-9",
				  "sex": "女",
				  "city": "城市-9",
				  "sign": "签名-9",
				  "experience": 484,
				  "logins": 25,
				  "wealth": 86801934,
				  "classify": "词人",
				  "score": 75
				}
			  ]
			}
		return JsonResponse(jsdata)

class ClearJson(LoginRequiredMixin, TemplateView):
	template_name = 'index.html'

	def get(self, request):
		jsdata = {
		  "code": 1,
		  "msg": "服务端清理缓存成功"
		}
		return JsonResponse(jsdata)
